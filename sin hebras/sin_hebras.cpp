#include <fstream>
#include <bits/stdc++.h>
#include <iostream>
#include "contador.h"
#include <chrono>
using namespace std;

int main(int argc, char **argv){
    
    auto start = chrono::system_clock::now();
    Contador contador;
    for(int a=1 ;a<argc;a++ ){
      contador.procesar_archivo(argv[a]);
      
      cout << endl;
    }
    contador.imprimir_total();
   
    auto end = chrono::system_clock::now();
    chrono::duration<float,milli> duration = end - start;

    cout << duration.count() << "s "<< endl;


  
  return 0;
}