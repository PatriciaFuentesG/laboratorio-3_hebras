#ifndef CONTADOR_H
#define CONTADOR_H

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
using namespace std;


class Contador {
    private:
     int cnt_pal = 0;
     int cnt_car = 0;
     int cnt_lin = 0;

    public:
        // default constructor
        Contador();
  
        void procesar_archivo(string archivo);
        int get_cnt_pal();
        void set_cnt_pal(int cnt);
        int get_cnt_car();
        void set_cnt_car(int cant);
        int get_cnt_lin();
        void set_cnt_lin(int cant);
        void contar(int cnt_pal, int cnt_car,int cnt);
        void imprimir_total();
        
    
};

#endif