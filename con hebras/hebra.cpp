#include <iostream>
#include <fstream>
#include <bits/stdc++.h>
#include <pthread.h>
#include <chrono>
#include<cstring>

using namespace std;

typedef struct{
    string archivo ;
    int lineas ;
    int caracteres ;
    int palabras ;
}dato_t;


void * procesar_archivo(void *param){
    
    dato_t *dato;
    dato = (dato_t *)param;
    string archivo;
    archivo = dato->archivo;
    ifstream file(archivo);
    string str,T;
    int cnt=0;
    int cnt_car =0;
    int cnt_pal = 0;

    while(getline(file,str)){
        cnt++;
        stringstream X(str); 
         
        //contar caracteres de la linea
         for(int i = 0; i < str.length(); i++){
            
             cnt_car++;
         } 
         //contar cantidad de palabras
        while( getline(X, T, ' ')){
           
            cnt_pal++;

         }

    }


    dato->caracteres = cnt_car;
    dato->lineas = cnt;
    dato->palabras = cnt_pal;

    cout <<"                               "<<endl;
    cout << dato->archivo << endl;
    cout << "palabras:"<<cnt_pal<<endl;
    cout << "lineas:"<< cnt<<endl;
    cout << "caracteres:"<< cnt_car <<endl;
    cout <<"                               "<<endl;
    
   pthread_exit(0);

}





int main(int argc,char *argv[]){
    auto start = chrono::system_clock::now();
    cout << "inicio"<<endl;
    pthread_t threads[argc - 1];
    //vector con estructura
    dato_t datos[argc-1];
    int total_lin = 0;
    int total_pal = 0;
    int total_car = 0;
    int i;

   
    for(int i=1;i<argc ;i++){
       
      datos[i-1].archivo = argv[i];
      datos[i-1].caracteres = 0;
      datos[i-1].palabras = 0;
      datos[i-1].lineas = 0;

      pthread_create(&threads[i], NULL, procesar_archivo, (void*)&datos[i-1]);
     
    }

    

    // Para esperar por el término de todos los hilos 
    for (i=1; i< argc; i++) {
        pthread_join(threads[i], NULL);
        total_lin += datos[i-1].lineas;
        total_car += datos[i-1].caracteres;
        total_pal += datos[i-1].palabras;
    
    }
    
    //imprimir totales 
    cout << "\nTotales\n"<< endl;
    cout << "Lineas: "<< total_lin <<endl;
    cout << "Caracteres: "<< total_car<< endl; 
    cout << "Palabras: "<<total_pal<<endl;

    auto end = chrono::system_clock::now();
    chrono::duration<float,milli> duration = end - start;

     cout << duration.count() << "s "<< endl;

     return 0;

}